#include<stdio.h>
int main()
{
    int x,y;
	printf("Enter 2 numbers:\n");
	scanf("%d%d",&x,&y);
	printf("\nValues before swapping:\n x=%d y=%d",x,y);
	swap(&x,&y);
	printf("\nValues after swapping:\n x=%d y=%d",x,y);
}
int swap(int* i,int* j)
{
	int temp=*i;
	*i=*j;
	*j=temp;
    return 0;
}