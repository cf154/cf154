#include <stdio.h>
int main()
{
    FILE * f;
    char c;
    printf("Enter the input:\n");
    f = fopen("INPUT.txt", "w");
    while ((c = getchar()) != EOF)
    {
        putc(c, f);
    }
    fclose(f);
    printf("Data Entered:\n");
    f = fopen("INPUT.txt", "r");
    while ((c = getc(f)) != EOF)
    {
        printf("%c", c);
    }
    fclose(f);
    return 0;
}