//This program includes all the array operations(both 1D and 2D):
#include<stdio.h>
#include<stdlib.h>
int Insert();
int Delete();
int LSearch();
int BSearch();
int Transpose();
int Sum();
int Difference();
int main()
{
 int n,x,y;
 printf("Enter your desired operation:(1/2)\n1)1D Array Operation\n2)2D Array Operation");
 scanf("%d",&n);
 system("cls");
 switch(n)
 { 
       case 1: printf("\nEnter the next operation:(1/2/3/4)\n1)Insert an element\n2)Delete an element\n3)Linear Search\n4)Binary Search");
               scanf("%d",&x);
               if(x==1) Insert();
               else if(x==2) Delete();
               else if(x==3) LSearch();
               else if(x==4) BSearch();
               else printf("\nInvalid!");break;
       
       case 2: printf("\nEnter the next operation:(1/2/3)\n1)Transpose of matrix\n2)Sum of two matrix\n3)Difference of two matrix");
               scanf("%d",&y);
               if(y==1) Transpose();
               else if(y==2) Sum();
               else if(y==3) Difference();
               else printf("\nInvalid!");break;
       
       default:printf("\nInvalid");break;
       
 }
 return 0;
}
int Insert()
{
 int a[50],n,item,pos
 printf("\nEnter the size of the array:\n");
 scanf("%d",&n);
 printf("\nEnter the array(in asc order):\n");
 for(int i=0;i<n;i++)
     scanf("%d",&a[i]);
 printf("\nEnter the element to be inserted");
 scanf("%d",&item);