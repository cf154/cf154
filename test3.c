#include<stdio.h>
#include<math.h>
int main()
{
int a,b,c;
printf("Enter a,b,c from the quadratic equation:\n");
scanf("%d%d%d",&a,&b,&c);
float D=((b*b)-(4*a*c));
if(a==0)
printf("INVALID EQUATION");
if(D==0)
{
int r=-b/(2*a);
printf("\nThe roots are equal and real and is: %f",r);
}
else if(D>0)
{
float r1=(-b+sqrt(D))/(2*a);
float r2=(-b-sqrt(D))/(2*a);
printf("\nThe roots are distinct and real: %f%f",r1,r2);
}
else if(D<0)
{
float r1=(-b/(2*a));
float r2=sqrt(abs(D))/(2*a);
printf("\nThe roots are distinct and complex: %f+i%f \n %f-i%f",r1,r2,r1,r2);
}
return 0;
}
