#include<stdio.h>
#include<stdlib.h>
struct stud
{
    int roll, fees;
    char name[20], dept[20], sec;
    float results;
};
int main()
{
    struct stud S1,S2;
    printf("Enter the details of student 1: (in the order of roll, fees, name, dept, section and results)\n");
    scanf(" %d %d %s %s %c %f",&S1.roll,&S1.fees,&S1.name,&S1.dept,&S1.sec,&S1.results);
    printf("\nEnter the details of student 2: (similar order)\n");
    scanf(" %d %d %s %s %c %f",&S2.roll,&S2.fees,&S2.name,&S2.dept,&S2.sec,&S2.results);
    system("cls");
    if(S1.results>S2.results)
      printf("\nStudent 1 has scored more and his details are as follows:\n1)Roll:%d\n2)Fees:%d\n3)Name:%s\n4)Department:%s\n5)Section:%c\n6)Results:%f",S1.roll,S1.fees,S1.name,S1.dept,S1.sec,S1.results);
    else if(S2.results>S1.results)
      printf("\nStudent 2 has scored more and his details are as follows:\n1)Roll:%d\n2)Fees:%d\n3)Name:%s\n4)Department:%s\n5)Section:%c\n6)Results:%f",S2.roll,S2.fees,S2.name,S2.dept,S2.sec,S2.results);
    else
      printf("\nBoth the students have secured the same results.");
return 0;
}