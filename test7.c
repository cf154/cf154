
#include<stdio.h>
int main()
{
 int a[50][50],x,y,transpose[50][50];
 printf("Enter the number of rows and columns:\n");
 scanf("%d %d",&x,&y);
 printf("\nEnter the matrix elements row wise:\n");
 for(int i=0;i<x;i++)
 {   for(int j=0;j<y;j++)
         scanf("%d",&a[i][j]);
 }
 for(int i=0;i<x;i++)
 {  for(int j=0;j<y;j++)
        transpose[j][i]=a[i][j];
 }
 printf("\nTransposed Matrix:\n");
 for(int i=0;i<y;i++)
 {   for(int j=0;j<x;j++)
         printf("%d ",transpose[i][j]);
	 printf("\n");
 }
 return 0;
}