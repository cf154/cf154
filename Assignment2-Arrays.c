#include<stdio.h>
int BSearch(int[],int,int);
int main()
{
 int a[20],n,index,item;
 printf("Enter the size of array\n");
 scanf("%d",&n);
 printf("\nEnter the members of array in ascending order\n");
 for(int i=0;i<n;i++)
    scanf("%d",&a[i]);
 printf("\nEnter the item to be searched in the array:\n");
 scanf("%d",&item);
 index=BSearch(a,n,item);
 if(index==-1)
   printf("\nArray item not found!");
 else 
   printf("\nArray item found in index: %d and position: %d",index,(index+1));
 return 0;
}
int BSearch(int a[],int n,int item)
{
 int beg,mid,last;
 beg=0;last=n-1;
 while(beg<=last)
 { mid=(beg+last)/2;
   if(a[mid]==item) return mid;
   else if(item>a[mid]) beg=mid+1;
   else last=mid-1;
 }
 return -1;
}

